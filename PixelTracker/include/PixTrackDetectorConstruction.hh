//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file PixTrackDetectorConstruction.hh
/// \brief Definition of the PixTrackDetectorConstruction class

#ifndef PixTrackDetectorConstruction_h
#define PixTrackDetectorConstruction_h 1

#include "globals.hh"
#include "G4GDMLParser.hh"
#include "G4VUserDetectorConstruction.hh"
#include "tls.hh"

class G4VPhysicalVolume;
class G4LogicalVolume;
class G4Material;
class G4UserLimits;
class G4GlobalMagFieldMessenger;

class PixTrackDetectorMessenger;

/// Detector construction class to define materials, geometry
/// and global uniform magnetic field.

class PixTrackDetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    PixTrackDetectorConstruction();
    virtual ~PixTrackDetectorConstruction();

  public:
    virtual G4VPhysicalVolume* Construct();
    virtual void ConstructSDandField();

    // Set methods
    void SetTargetMaterial (G4String );
    void SetSensorMaterial(G4String );
    void SetMaxStep (G4double );
    void SetCheckOverlaps(G4bool );

  private:
    // methods
    void DefineMaterials();
    G4VPhysicalVolume* DefineVolumes();
  
    // data members
    G4int fNbOfPlanes;
    G4bool fWriteFile;
    G4String fOutFileName;
    G4VPhysicalVolume* fWorldPhysVol;

    G4LogicalVolume*  fLogicTarget;
    G4LogicalVolume*  fLogicFrame;
    // G4LogicalVolume*  fLogicModuleThin;
    // G4LogicalVolume*  fLogicModuleThick;
    G4LogicalVolume*  fLogicPlane;
    G4LogicalVolume*  fLogicFrontEnd;
    G4LogicalVolume*  fLogicSensorThin;
    G4LogicalVolume*  fLogicSensorThick;
    G4LogicalVolume*  fLogicFlex;

    G4Material*        fDefaultMaterial;
    G4Material*        fTargetMaterial;
    G4Material*        fFrontEndMaterial;
    G4Material*        fSensorMaterial;
    G4Material*        fFrameMaterial;
    G4Material*        fFlexMaterial;
    // G4Material*        fModuleMaterial1;
    // G4Material*        fModuleMaterial2;

    G4UserLimits* fStepLimit;            // pointer to user step limits

    PixTrackDetectorMessenger*  fMessenger;   // messenger
    G4GDMLParser fParser;

    static G4ThreadLocal G4GlobalMagFieldMessenger*  fMagFieldMessenger; 
                                         // magnetic field messenger
    
    G4bool  fCheckOverlaps; // option to activate checking of volumes overlaps 
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
