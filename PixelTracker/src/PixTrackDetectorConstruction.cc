//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file PixTrackDetectorConstruction.cc
/// \brief Implementation of the PixTrackDetectorConstruction class

#include "PixTrackDetectorConstruction.hh"
#include "PixTrackDetectorMessenger.hh"
#include "PixTrackModuleParameterisation.hh"
#include "PixTrackSD.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4SDManager.hh"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVParameterised.hh"
#include "G4GlobalMagFieldMessenger.hh"
#include "G4AutoDelete.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4IntersectionSolid.hh"

#include "G4GeometryTolerance.hh"
#include "G4GeometryManager.hh"

#include "G4UserLimits.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4GDMLParser.hh"

#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4ThreadLocal
    G4GlobalMagFieldMessenger *PixTrackDetectorConstruction::fMagFieldMessenger = 0;

PixTrackDetectorConstruction::PixTrackDetectorConstruction()
    : G4VUserDetectorConstruction(),
      fNbOfPlanes(0),
      fLogicTarget(NULL),
      fTargetMaterial(NULL), fSensorMaterial(NULL), fFrontEndMaterial(NULL), fFlexMaterial(NULL),
      fStepLimit(NULL),
      fCheckOverlaps(true)
{
  fMessenger = new PixTrackDetectorMessenger(this);

  fNbOfPlanes = 6;
  fWriteFile = true;
  fOutFileName = "./pixelTracker.gdml";
  // TODO add solderbumps?
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PixTrackDetectorConstruction::~PixTrackDetectorConstruction()
{
  delete fLogicFlex;
  delete fLogicFrame;
  delete fLogicFrontEnd;
  delete fLogicPlane;
  delete fLogicTarget;
  delete fLogicSensorThick;
  delete fLogicSensorThin;
  delete fStepLimit;
  delete fMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume *PixTrackDetectorConstruction::Construct()
{
  // Define materials
  DefineMaterials();

  // Define volumes
  fWorldPhysVol = DefineVolumes();
  if (fWriteFile)
    fParser.Write(fOutFileName, fWorldPhysVol);
  return fWorldPhysVol;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PixTrackDetectorConstruction::DefineMaterials()
{
  // Material definition

  G4NistManager *nistManager = G4NistManager::Instance();

  fDefaultMaterial = nistManager->FindOrBuildMaterial("G4_Galactic");

  // Air defined using NIST Manager
  nistManager->FindOrBuildMaterial("G4_AIR");

  // Lead defined using NIST Manager
  fTargetMaterial = nistManager->FindOrBuildMaterial("G4_Pb");
  fSensorMaterial = nistManager->FindOrBuildMaterial("G4_Si");
  fFrontEndMaterial = nistManager->FindOrBuildMaterial("G4_Si");
  fFrameMaterial = nistManager->FindOrBuildMaterial("G4_Al");

  G4Material *copper = nistManager->FindOrBuildMaterial("G4_Cu");
  G4Material *kapton = nistManager->FindOrBuildMaterial("G4_KAPTON");
  G4double flexdensity = copper->GetDensity() * 36. / 150. + kapton->GetDensity() * 114 / 150;

  fFlexMaterial = new G4Material("Flex", flexdensity, 2);
  fFlexMaterial->AddMaterial(kapton, 0.76);
  fFlexMaterial->AddMaterial(copper, 0.24);

  // G4double flexThickness = 150. * um;
  // G4double frontEndThickness = 150. * um;
  // G4double sensorThickness1 = 200. * um;
  // G4double sensorThickness2 = 250. * um;

  // G4double module_thickness1 = flexThickness + frontEndThickness + sensorThickness1;
  // G4double module_thickness2 = flexThickness + frontEndThickness + sensorThickness2;

  // G4double moduleDensity1 = fFlexMaterial->GetDensity() * flexThickness / module_thickness1 + fSensorMaterial->GetDensity() * sensorThickness1 / module_thickness1 + fFrontEndMaterial->GetDensity() * frontEndThickness / module_thickness1;
  // G4double moduleDensity2 = fFlexMaterial->GetDensity() * flexThickness / module_thickness2 + fSensorMaterial->GetDensity() * sensorThickness2 / module_thickness2 + fFrontEndMaterial->GetDensity() * frontEndThickness / module_thickness2;

  // Print materials
  G4cout << *(G4Material::GetMaterialTable()) << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume *PixTrackDetectorConstruction::DefineVolumes()
{
  G4Material *air = G4Material::GetMaterial("G4_AIR");

  // Sizes of the principal geometrical components (solids)

  G4double frameThickness = 0.6 * cm; 
  G4double planeDistance = 2.1 * cm ; 
  G4double frameWidth = 11.4 * cm;
  G4double frameHeight = 11.4 * cm;
  G4double cutOutWidth = 37. * mm;
  G4double cutOutHeight = 33. * mm;

  G4double pixelMatrixSizeY = 50*um * 336;
  G4double sensorWidth = 41.4 * mm;  // TODO: add guard rings etc. right now only pixel size is considered
  G4double sensorHeight = pixelMatrixSizeY + 2*0.75*mm; // TODO: add guard rings etc. right now only pixel size is considered
  G4double sensorThickness1 = 200. * um;
  G4double sensorThickness2 = 250. * um;

  G4double frontEndWidth = 20.27 * mm;
  G4double frontEndHeight = 19.2 * mm;
  G4double frontEndThickness = 150. * um;

  G4double flexThickness = 150. * um;

  G4double planeThickness = frameThickness + 2*flexThickness + 2*frontEndThickness + 2*sensorThickness2;
  G4double moduleThickness = flexThickness + frontEndThickness + sensorThickness2;

  G4double targetLength = 5.0 * cm; // full length of Target
  G4double targetWidth = 12. * cm;
  G4double targetHeight = 10. * cm;

  G4double alignYModule0 = -0.1*mm; //-1.3*mm + 0.2*mm;
  G4double alignYModule1 = 0.4*mm; //-0.1*mm - 0.2*mm;
  G4double alignPlaneZ = 1.8 * cm;

  G4double trackerLength = (fNbOfPlanes-1)* planeDistance + 2*moduleThickness +  fNbOfPlanes*frameThickness;

  G4double worldLength = 1.2 * (2 * targetLength + 2 * frameWidth);

  G4double trackerSize = 0.5 * trackerLength; // Half length of the Tracker
  

  // Definitions of Solids, Logical Volumes, Physical Volumes

  // World

  G4GeometryManager::GetInstance()->SetWorldMaximumExtent(worldLength);

  G4cout << "Computed tolerance = "
         << G4GeometryTolerance::GetInstance()->GetSurfaceTolerance() / mm
         << " mm" << G4endl;
  // for(G4int i=0;i<20;i++){
  //   G4cout << "calculate tracker length to  = "
  //         << trackerLength / mm
  //         << " mm" << G4endl;
  // }
  G4Box *worldS = new G4Box("world",                                               //its name
                            worldLength / 2., worldLength / 2., worldLength / 2.); //its size
  G4LogicalVolume *worldLV = new G4LogicalVolume(
      worldS,   //its solid
      air,      //its material
      "World"); //its name

  //  Must place the World Physical volume unrotated at (0,0,0).
  //
  G4VPhysicalVolume *worldPV = new G4PVPlacement(
      0,               // no rotation
      G4ThreeVector(), // at (0,0,0)
      worldLV,         // its logical volume
      "World",         // its name
      0,               // its mother  volume
      false,           // no boolean operations
      0,               // copy number
      fCheckOverlaps); // checking overlaps

  // Target

  G4ThreeVector positionTarget = G4ThreeVector(0, 0, -(targetLength + trackerSize));

  G4Box *targetS = new G4Box("target", targetHeight, targetWidth, targetLength);
  fLogicTarget = new G4LogicalVolume(targetS, fTargetMaterial, "Target", 0, 0, 0);
  new G4PVPlacement(0,               // no rotation
                    positionTarget,  // at (x,y,z)
                    fLogicTarget,    // its logical volume
                    "Target",        // its name
                    worldLV,         // its mother volume
                    false,           // no boolean operations
                    0,               // copy number
                    fCheckOverlaps); // checking overlaps

  G4cout << "Target is " << 2 * targetLength / cm << " cm of "
         << fTargetMaterial->GetName() << G4endl;

  // Tracker

  G4ThreeVector positionTracker = G4ThreeVector(0, 0, alignPlaneZ);

  G4Box *trackerS = new G4Box("tracker", frameWidth, frameHeight, trackerSize);
  G4LogicalVolume *trackerLV = new G4LogicalVolume(trackerS, air, "Tracker", 0, 0, 0);
  new G4PVPlacement(0,               // no rotation
                    positionTracker, // at (x,y,z)
                    trackerLV,       // its logical volume
                    "Tracker",       // its name
                    worldLV,         // its mother  volume
                    false,           // no boolean operations
                    0,               // copy number
                    fCheckOverlaps); // checking overlaps

  G4double planeZStart = -trackerS->GetZHalfLength() + planeThickness/2.; 

  G4cout 
  << " tracker full length " << trackerLength << G4endl
  << " plane thickness " << planeThickness << G4endl
  << "first plane starts at " << planeZStart << G4endl;

  // Frames

  G4Box *frameS = new G4Box("frame", frameWidth / 2., frameHeight / 2., frameThickness / 2.);
  G4Box *cutOutS = new G4Box("cutOut", cutOutWidth / 2., cutOutHeight / 2., frameThickness / 2.);
  G4VSolid *frameCutOutS = new G4SubtractionSolid("frameCutOut", frameS, cutOutS );

  fLogicFrame = new G4LogicalVolume(frameCutOutS, fFrameMaterial, "frame", 0, 0, 0);
  // Visualization attributes

  G4VisAttributes *boxVisAtt = new G4VisAttributes(G4Colour(1.0, 1.0, 1.0));
  G4VisAttributes *targetVisAtt = new G4VisAttributes(G4Colour(0.83, 0.84, 0.44));
  G4VisAttributes *sensorVisAtt = new G4VisAttributes(G4Colour(0.498, 0.553, 0.612));
  G4VisAttributes *flexVisAtt = new G4VisAttributes(G4Colour(0.808, 0.506, 0.031));
  G4VisAttributes *frontEndVisAtt = new G4VisAttributes(G4Colour(0.419608, 0.262745, 0.0431373));
  G4VisAttributes *frameVisAtt = new G4VisAttributes(G4Colour(0.498, 0.553, 0.612));

  worldLV->SetVisAttributes(boxVisAtt);
  fLogicTarget->SetVisAttributes(targetVisAtt);
  trackerLV->SetVisAttributes(boxVisAtt);
  fLogicFrame->SetVisAttributes(frameVisAtt);

  // Tracker segments

  G4cout << "There are " << fNbOfPlanes << " planes in the tracker region. "
         << G4endl
         << "The sensors are " << sensorThickness1/mm << " mm of "
         << fSensorMaterial->GetName() << " each, " << (sensorThickness1 * fNbOfPlanes)/mm << " mm in total. " << G4endl
         << "The distance between planes is " << planeDistance /cm << " cm"
         << G4endl;

  // G4double ZpositionSensor_front = planeZ + flexThickness + sensorThickness1 / 2;
  // G4double ZpositionSensor_back = ZpositionSensor_front + frameThickness;

  // G4Box *modulebox_thin = new G4Box("module_thin", frameWidth / 2., frameHeight / 2., moduleThickness/2.);
  // G4Box *modulebox_thick = new G4Box("module_thick", frameWidth / 2., frameHeight / 2., planeThickness/2.);
  // fLogicModuleThin = new G4LogicalVolume(modulebox_thin, fDefaultMaterial, "module_thin", 0, 0, 0);
  // fLogicModuleThick = new G4LogicalVolume(modulebox_thick, fDefaultMaterial, "module_thick", 0, 0, 0);

  G4Box *planeS = new G4Box("plane", frameWidth / 2., frameHeight / 2., planeThickness/2.);
  fLogicPlane = new G4LogicalVolume(planeS, fDefaultMaterial, "plane", 0, 0, 0);

  // G4Box *planeS = new G4Box("plane", frame)
  // fLogigPlane = new G4LogicalVolume(planeS, fDefaultMaterial, "plane", 0,0,0);

  G4Box *frontEndBox = new G4Box("frontEndLV", frontEndWidth / 2., frontEndHeight / 2., frontEndThickness / 2.);
  fLogicFrontEnd = new G4LogicalVolume(frontEndBox, fFrontEndMaterial, "frontEnd", 0, 0, 0);

  G4Box *sensorBox1 = new G4Box("sensor1LV", sensorWidth / 2., sensorHeight / 2., sensorThickness1 / 2.);
  fLogicSensorThin = new G4LogicalVolume(sensorBox1, fSensorMaterial, "sensor1", 0, 0, 0);
  
  G4Box *sensorBox2 = new G4Box("sensor2LV", sensorWidth / 2., sensorHeight / 2., sensorThickness2 / 2.);
  fLogicSensorThick = new G4LogicalVolume(sensorBox2, fSensorMaterial, "sensor2", 0, 0, 0);
  
  G4Box *flexBox = new G4Box("frontEnd", sensorWidth / 2., sensorHeight / 2., flexThickness / 2.);
  fLogicFlex = new G4LogicalVolume(flexBox, fFlexMaterial, "flex", 0, 0, 0);

  fLogicSensorThick->SetVisAttributes(sensorVisAtt);
  fLogicFrontEnd->SetVisAttributes(frontEndVisAtt);
  fLogicFlex->SetVisAttributes(flexVisAtt);

  G4RotationMatrix* module0RotMat = new G4RotationMatrix();
  module0RotMat->rotateX(0.*deg);
  module0RotMat->rotateY(0.*deg);
  module0RotMat->rotateZ(0.*deg);
  
  G4RotationMatrix* module1RotMat = new G4RotationMatrix();
  module1RotMat->rotateX(0.*deg);
  module1RotMat->rotateY(180.*deg);
  module1RotMat->rotateZ(0.*deg);

  G4double frontEndX0 = -frontEndWidth / 2. - 50.*um; 
  G4double frontEndX1 = frontEndWidth / 2. + 50*um; 

  G4double frontEndY0 = alignYModule0 + frontEndHeight / 2. - (frontEndHeight - pixelMatrixSizeY); 
  G4double frontEndY1 = alignYModule1 - frontEndHeight / 2. + (frontEndHeight - pixelMatrixSizeY);

  G4double frontEndZ =  frameThickness/2. + frontEndThickness/2.;

  G4double sens0Y = alignYModule0 + sensorHeight/2.;
  G4double sens1Y = alignYModule1 - sensorHeight/2.;
  G4double sensorZ = frontEndZ + sensorThickness2/2. + frontEndThickness/2.;

  G4double flex0Y = alignYModule0 + sensorHeight / 2. ;
  G4double flex1Y = alignYModule1 - sensorHeight / 2. ;
  G4double flexZ =sensorZ + flexThickness/2. + sensorThickness2/2.;

  new G4PVPlacement(module0RotMat, G4ThreeVector(0., -flex0Y, -flexZ), fLogicFlex, "flex0", fLogicPlane, false, 0, fCheckOverlaps);
  new G4PVPlacement(module0RotMat, G4ThreeVector(0., -sens0Y, -sensorZ), fLogicSensorThin, "sensor0", fLogicPlane, false, 0, fCheckOverlaps);
  new G4PVPlacement(module0RotMat, G4ThreeVector(-frontEndX0, -frontEndY0, -frontEndZ), fLogicFrontEnd, "frontEnd0_0", fLogicPlane, false, 0, fCheckOverlaps);
  new G4PVPlacement(module0RotMat, G4ThreeVector(-frontEndX1, -frontEndY0, -frontEndZ), fLogicFrontEnd, "frontEnd1_0", fLogicPlane, false, 0, fCheckOverlaps);

  new G4PVPlacement(module1RotMat, G4ThreeVector( 0.,-flex1Y , flexZ), fLogicFlex, "flex1", fLogicPlane, false, 0, fCheckOverlaps);
  new G4PVPlacement(module1RotMat, G4ThreeVector(0., -sens1Y, sensorZ), fLogicSensorThin, "sensor1", fLogicPlane, false, 0, fCheckOverlaps);
  new G4PVPlacement(module1RotMat, G4ThreeVector(-frontEndX0, -frontEndY1, frontEndZ), fLogicFrontEnd, "frontEnd0_1", fLogicPlane, false, 0, fCheckOverlaps);
  new G4PVPlacement(module1RotMat, G4ThreeVector(-frontEndX1,- frontEndY1, frontEndZ), fLogicFrontEnd, "frontEnd1_1", fLogicPlane, false, 0, fCheckOverlaps);

  new G4PVPlacement(0, G4ThreeVector(0,0,0),fLogicFrame, "frame", fLogicPlane, false, 0, fCheckOverlaps);

  G4VPVParameterisation *planeParam = new PixTrackModuleParameterisation(fNbOfPlanes, planeZStart, planeDistance, planeThickness, sensorHeight, frameThickness);
  new G4PVParameterised("module", fLogicPlane, trackerLV, kZAxis, fNbOfPlanes, planeParam, fCheckOverlaps);

  // for(G4int i=0;i<25;i++){
  // G4cout << "z positions: " << G4endl 
  //         << "          frontEnd1 " << frontEndZ << G4endl
  //         << "          sensor1 " << sensorZ << G4endl
  //         << "          flex1 " << flexZ << G4endl;
  // }
  if (fNbOfPlanes > 0)
  {
    if (planeDistance < sensorThickness1)
    {
      G4Exception("PixTrackDetectorConstruction::DefineVolumes()",
                  "InvalidSetup", FatalException,
                  "Width>Spacing");
    }
  }

  // Example of User Limits
  //
  // Below is an example of how to set tracking constraints in a given
  // logical volume
  //
  // Sets a max step length in the tracker region, with G4StepLimiter

  G4double maxStep = 0.5 * sensorThickness2;
  fStepLimit = new G4UserLimits(maxStep);
  trackerLV->SetUserLimits(fStepLimit);

  /// Set additional contraints on the track, with G4UserSpecialCuts
  ///
  // / G4double maxLength = 2*trackerLength, maxTime = 0.1*ns, minEkin = 10*MeV;
  // / trackerLV->SetUserLimits(new G4UserLimits(maxStep,
  // /                                           maxLength,
  // /                                           maxTime,
  // /                                           minEkin));

  // Always return the physical world

  return worldPV;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PixTrackDetectorConstruction::ConstructSDandField()
{
  // Sensitive detectors

  G4String trackerChamberThinSDname = "PixTrack/TrackerChamberThinSD";
  PixTrackSD *aTrackerThinSD = new PixTrackSD(trackerChamberThinSDname, "TrackerHitsCollection1");
  G4SDManager::GetSDMpointer()->AddNewDetector(aTrackerThinSD);
  // Setting aTrackerThinSD to all logical volumes with the same name of "Chamber_LV".
  SetSensitiveDetector("sensor1", aTrackerThinSD, true);

  G4String trackerChamberThickSDname = "PixTrack/TrackerChamberThickSD";
  PixTrackSD *aTrackerThickSD = new PixTrackSD(trackerChamberThickSDname, "TrackerHitsCollection2");
  G4SDManager::GetSDMpointer()->AddNewDetector(aTrackerThickSD);
  // Setting aTrackerThickSD to all logical volumes with the same name of "Chamber_LV".
  SetSensitiveDetector("sensor2", aTrackerThickSD, true);

  // Create global magnetic field messenger.
  // Uniform magnetic field is then created automatically if the field value is not zero.
  G4ThreeVector fieldValue = G4ThreeVector();
  fMagFieldMessenger = new G4GlobalMagFieldMessenger(fieldValue);
  fMagFieldMessenger->SetVerboseLevel(1);

  // Register the field messenger for deleting
  G4AutoDelete::Register(fMagFieldMessenger);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PixTrackDetectorConstruction::SetTargetMaterial(G4String materialName)
{
  G4NistManager *nistManager = G4NistManager::Instance();

  G4Material *pttoMaterial =
      nistManager->FindOrBuildMaterial(materialName);

  if (fTargetMaterial != pttoMaterial)
  {
    if (pttoMaterial)
    {
      fTargetMaterial = pttoMaterial;
      if (fLogicTarget)
        fLogicTarget->SetMaterial(fTargetMaterial);
      G4cout
          << G4endl
          << "----> The target is made of " << materialName << G4endl;
    }
    else
    {
      G4cout
          << G4endl
          << "-->  WARNING from SetTargetMaterial : "
          << materialName << " not found" << G4endl;
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PixTrackDetectorConstruction::SetSensorMaterial(G4String materialName)
{
  G4NistManager *nistManager = G4NistManager::Instance();

  G4Material *pttoMaterial =
      nistManager->FindOrBuildMaterial(materialName);

  if (fSensorMaterial != pttoMaterial)
  {
    if (pttoMaterial)
    {
      fSensorMaterial = pttoMaterial;
      fLogicSensorThick->SetMaterial(fSensorMaterial);
      G4cout
          << G4endl
          << "----> The modules are made of " << materialName << G4endl;
    }
    else
    {
      G4cout
          << G4endl
          << "-->  WARNING from SetSensorMaterial : "
          << materialName << " not found" << G4endl;
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PixTrackDetectorConstruction::SetMaxStep(G4double maxStep)
{
  if ((fStepLimit) && (maxStep > 0.))
    fStepLimit->SetMaxAllowedStep(maxStep);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PixTrackDetectorConstruction::SetCheckOverlaps(G4bool checkOverlaps)
{
  fCheckOverlaps = checkOverlaps;
}
