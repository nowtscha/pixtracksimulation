//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file PixTrackModuleParameterisation.cc
/// \brief Implementation of the PixTrackModuleParameterisation class

#include "PixTrackModuleParameterisation.hh"

#include "G4VPhysicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Box.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PixTrackModuleParameterisation::PixTrackModuleParameterisation(
    G4int noPlanes,
    G4double planeZStart,
    G4double planeSpacing,
    G4double planeThickness,
    G4double sensorHeight,
    G4double frameThickness)
    : G4VPVParameterisation()
{
   fNoPlanes = noPlanes;
   fplaneZStart = planeZStart;
   fPlaneSpacing = planeSpacing;
   fplaneThickness = planeThickness;
   fFrameThickness = frameThickness;
   fsensorHeight = sensorHeight;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PixTrackModuleParameterisation::~PixTrackModuleParameterisation()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PixTrackModuleParameterisation::ComputeTransformation(const G4int copyNo,  G4VPhysicalVolume *physVol) const
{
   G4double x_pos;
   G4double y_pos;
   G4double z_pos;
   G4double x_rot;
   G4double y_rot;
   G4double z_rot;
   x_pos = 0. * mm;
   x_rot = 0. * deg;
   y_rot = 0. * deg;

   z_pos = copyNo * (fPlaneSpacing + fFrameThickness) +  fplaneZStart;

   if (copyNo % 2 == 0)
   {
      y_pos = 0. * mm;
      z_rot = 270. * deg;
   }
   else if (copyNo % 2 == 1)
   {
      y_pos = 0. * mm;
      z_rot = 0. * deg;
   }

   G4ThreeVector translation(x_pos, y_pos, z_pos);
   G4RotationMatrix *rotmat = new G4RotationMatrix();
   rotmat->rotateX(x_rot);
   rotmat->rotateY(y_rot);
   rotmat->rotateZ(z_rot);
   physVol->SetTranslation(translation);
   physVol->SetRotation(rotmat);
   
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PixTrackModuleParameterisation::ComputeDimensions(G4Box &trackerPlane, const G4int copyNo, const G4VPhysicalVolume *) const
{
   trackerPlane.SetXHalfLength((11.4 * cm) / 2.);
   trackerPlane.SetYHalfLength((11.4 * cm) / 2.);
   trackerPlane.SetZHalfLength(fplaneThickness/2.);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
